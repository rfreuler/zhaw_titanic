# -*- coding: utf-8 -*-
"""
Created on 06 Nov 06.11.16 13:21 2016 

@author: Sebastian / Roger

Description:
This Script reads a csv export of the Titanic passenger list.
It will read a training file (survival is known) and generates a prediction rate
of how likely a passenger would survive out of a test file (survival is unknown).
Output must be:
----
key;value
100001;0
100002;1
100003;0
"""

import os
import csv
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier

pd.options.mode.chained_assignment = None  # default='warn'

# Definitions
output_file = "Team_FreulerMojadoX_WithTicketAndMore.csv"
training_file = "Roger/titanic3_train.csv"
test_file = "Roger/titanic3_test.csv"


def rmfile_if_exist(filename):
	"""
	checks if a file exists and removes it
	:param filename: string
	:return:
	"""
	if os.path.exists(filename):
		try:
			os.remove(filename)
		except OSError:
			pass


def write_output_file(filename, key, value):
	""" Match the predictions with the passenger ID of the test data and write the data into a csv file
		Format:
		key;value
		100001;0
		100002;1
		100003;0
	:param filename: string
	:param key: list
	:param value: list
	:return:
	"""
	predictions_file = open(filename, 'wb')
	open_file_object = csv.writer(predictions_file, delimiter=';')
	open_file_object.writerow(['key', 'value'])
	open_file_object.writerows(zip(key, value))
	predictions_file.close()

def get_median_ages(dataframe, count_gender, count_class):
	"""
	Creates a new numpy array which contains the median ages based on:
		- gender
		- class
	:param dataframe: pandas.DataFrame
	:param count_gender: int
	:param count_class: int
	:return: numpy.array
	"""
	median_ages = np.zeros((count_gender, count_class))
	for i in range(0, count_gender):  # for each gender
		for j in range(0, count_class):  # for each class in gender
			# print "Gender:", i, "Class:", j+1
			# print df_train[ (df_train.gender == i) & (df_train.pclass == j+1) ][["sex", "pclass", "age"]]
			# print df_train[(df_train.gender == i) & (df_train.pclass == j + 1)]["age"].dropna().median()
			median_ages[i, j] = dataframe[(dataframe.gender == i) & (dataframe.pclass == j + 1)][
				"age"].dropna().median()
	return median_ages


def switch_colums(dataframe, column1, column2):
	"""
	will switch two columns of a given DataFrame
	:param dataframe: pandas.DataFrame
	:param column1: string
	:param column2: string
	:return: dataframe
	"""
	columns = dataframe.columns.tolist()
	a, b = columns.index(column1), columns.index(column2)
	columns[b], columns[a] = columns[a], columns[b]
	return dataframe[columns]


def get_age(dataframe, dataframe_train_and_test):
	"""
	will receive a dataframe with empty values in aging column
		1: generates a median ages reference array
		2: will fill the empty ages value with the corresponding median ages value
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	# first, lets create a new reference table with median ages per gender and class
	count_class = len(dataframe_train_and_test["pclass"].unique())
	count_gender = len(dataframe_train_and_test["gender"].unique())
	median_ages = get_median_ages(dataframe_train_and_test, count_gender, count_class)
	# print median_ages

	# copy original age column to fill the null values and still see the original
	dataframe['agefill'] = dataframe['age']

	# fill null values with values from our median_ages table
	for i in range(0, count_gender):  # for female(0) and male(1)
		for j in range(0, count_class):  # for each pclass 1-3
			dataframe.loc[(dataframe.age.isnull()) & (dataframe.gender == i) & (dataframe.pclass == j + 1), "agefill"] = \
				median_ages[i, j]
		# print dataframe.loc[(dataframe.age.isnull()) & (dataframe.gender == i) & (dataframe.pclass == j+1)][["sex", "pclass", "age", "agefill"]]
	return dataframe


def get_sex(dataframe):
	"""
	Turning the female/male into 0/1 for further analysis
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	dataframe['gender'] = dataframe.sex.map({'female': 0, 'male': 1}).astype(int)
	return dataframe


def get_boat(dataframe):
	"""
	Reads the boat infos and fills a new column with 0/1 (found on lifeboat no/yes)
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	# create new lifeboat column
	dataframe['lifeboat'] = 0
	# fill empty values with 0
	dataframe.loc[(dataframe['boat'].isnull()), 'lifeboat'] = 0
	# fill all cells containing a value with 1
	dataframe.loc[(dataframe['boat'].notnull()), 'lifeboat'] = 1
	# print dataframe.loc[(dataframe['boat'].notnull())]
	dataframe['lifeboat'] = dataframe['lifeboat'].astype(int)
	# check result
	# print dataframe[0:100][['id', 'survived', 'boat', 'lifeboat']]
	return dataframe


def get_body(dataframe):
	"""
	Reads the body infos and fills a new column with 0/1 (found body no/yes)
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	# create new foundbody column
	dataframe['foundbody'] = 0
	# fill empty values with 0
	dataframe.loc[(dataframe['body'].isnull()), 'foundbody'] = 0
	# fill all cells containing a value with 1
	dataframe.loc[(dataframe['body'].notnull()), 'foundbody'] = 1
	# print dataframe.loc[(dataframe['boat'].notnull())]
	dataframe['foundbody'] = dataframe['foundbody'].astype(int)
	# check result
	# print dataframe[0:100][['id', 'survived', 'boat', 'lifeboat']]
	return dataframe


def get_id(dataframe):
	"""
	Reads the id infos and fills a new column with the id number
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	dataframe['foundid'] = dataframe['id']
	dataframe['foundid'] = dataframe['foundid'].astype(int)
	return dataframe


def get_title(dataframe):
	"""
	Reads the surname infos, extracts the titles and creates for each title a new category
	In addition to this there is a funny feature called "surname_letter_count"
	representing the count of letters of the surname without the title
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	all_found_titles = ["Ms.","Mr.","Mrs.","Miss.","Master.","Capt.","Col.","Major.","Jonkheer.","Don.","Sir.","Dr.","Rev.","the Countess.","Dona.","Mme.","Mlle.","Lady."]

	# generate titles categories
	for title in all_found_titles:
		titleCategoryMap = []
		for title_and_surname in dataframe["surname"]:
			if title in title_and_surname:
				titleCategoryMap.append(1)
			else:
				titleCategoryMap.append(0)
		feature_name = "title_"+title
		dataframe[feature_name] = titleCategoryMap

	# generate surname_letter_count feature
	surname_letter_count = []
	for title_and_surname in dataframe["surname"]:
		for title in all_found_titles:	
			if title in title_and_surname:
				surname_letter_count.append(len(title_and_surname)-len(title))
	dataframe['surname_letter_count'] = surname_letter_count
	return dataframe #checked output with variable explorer and excel. seems to work.

def get_cabin(dataframe):
	"""
	Separate Deck and Cabin information into separate columns and fill empty values
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	# create tuple and dataframe corresponding deck columns
	deck = ("a", "b", "c", "d", "e", "f", "g", "t")
	for d in deck:
		col_name = "deck_" + d
		dataframe[col_name] = 0		# create new deck colums
	dataframe['cabNr'] = 0 			# create new column with cabin number

	# Iterate through the DataFrame an generate the deck features
	for index, row in dataframe.iterrows():
		if (pd.isnull(row['cabin'])):			# ignore empty cabin values
			pass
		else:
			cabin_list = str(row['cabin']).lower().split(" ") # make infos lowercase and split into list
			if len(cabin_list) > 1: 			# deal with values with more than one entry
				cab_nr = []
				for cabin in cabin_list:		# take each cabin in list
					if not len(cabin) > 1:		# ignore single cabin values
						pass
					if cabin[:1] in deck:		# write deck
						dataframe.loc[index, "deck_" + cabin[:1]] = 1
					if cabin[1::]:				# get cabin number, read as int since we want to calculate the mean result
						cab_nr.append(int(cabin[1::]))
				dataframe.loc[index, 'cabNr'] = np.array(cab_nr).mean() # calculate mean and write into cabin number column
			else:
				if cabin_list[0][:1] in deck:	# write deck
					dataframe.loc[index, "deck_"+ cabin_list[0][:1]] = 1
				if cabin_list[0][1::]:			# write cabin number if we have one
					dataframe.loc[index, 'cabNr'] = cabin_list[0][1::]
	dataframe['cabNr'] = dataframe['cabNr'].astype(int)
 
	# Generate a feature that gives information whether there was information or not
	# Maybe survivors could give such cabin informations and dead passagers not
	dataframe['cabin_has_info'] = 0
	dataframe.loc[(dataframe['cabin'].isnull()), 'cabin_has_info'] = 0
	dataframe.loc[(dataframe['cabin'].notnull()), 'cabin_has_info'] = 1
	dataframe['cabin_has_info'] = dataframe['cabin_has_info'].astype(int)
 
	return dataframe
 
def get_embarked(dataframe):
	"""
	Separate embarked information into three categories (S,Q,C)
	Normally, a "no_embarked_info" category makes sense but there is only 
	1-2 passenger with this feature and therefore not relevant
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	# apperently, NaN gives an error with string contains. Therefore, it's replaced by Z
	dataframe.loc[(dataframe['embarked'].isnull()), 'embarked'] = 'Z'

	dataframe['embarked_C'] = 0
	dataframe.loc[(dataframe['embarked'].str.contains('C')), 'embarked_C'] = 1
	dataframe['embarked_C'] = dataframe['embarked_C'].astype(int)

	dataframe['embarked_Q'] = 0
	dataframe.loc[(dataframe['embarked'].str.contains('Q')), 'embarked_Q'] = 1
	dataframe['embarked_Q'] = dataframe['embarked_Q'].astype(int)

	dataframe['embarked_S'] = 0
	dataframe.loc[(dataframe['embarked'].str.contains('S')), 'embarked_S'] = 1
	dataframe['embarked_S'] = dataframe['embarked_S'].astype(int)
	return dataframe

class SurvivalQuotientPerTicketNr:
	def __init__(self): 
		self.survived = 0
		self.dead = 0
		self.unknown = 0
	
	def inc_survived(self):
		self.survived += 1

	def inc_dead(self):
		self.dead += 1
		
	def inc_unknown(self):
		self.unknown += 1

	def is_unknown_passenger(self):
		return (self.survived+self.dead)<1
	
	def get_quotient(self):
		if ((self.survived+self.dead)>=1):
			return self.survived / float(self.dead+self.survived)
		else:
			return 0.5

								
def get_ticket(dataframe, df_tofill):
	"""
	Find others with same number and calculate survival quotient
	The idea behind this is to identifiy groups and find out if these groups 
	together had a better chance for survival
	:param dataframe: pandas.DataFrame
	:return: dataframe, dataframe
        e.g.
			1 unique ticket number with surviving passenger:
				- ticket_same_ticket_count = 1
				- ticket_same_ticket_survived_quotient = 1
			3 same ticket number, 2 survived, 1 dead
				- ticket_same_ticket_count = 3
				- ticket_same_ticket_survived_quotient = 0.666
			4 same ticket number, 0 survived, 4 dead
				- ticket_same_ticket_count = 4
				- ticket_same_ticket_survived_quotient = 0
			5 same ticket number, 3 survived, 1 dead, 1 unknown  (unknown when row if from test data)
				- ticket_same_ticket_count = 5  (group size is getting larger)
				- ticket_same_ticket_survived_quotient = 0.75 (we ignore the unknown)
			1 same ticket number, 1 unknown  (unknown when row if from test data)
				- ticket_same_ticket_count = 1  
				- ticket_same_ticket_survived_quotient = 0.5 (single unknown is 50/50 chance)
			3 same ticket number, 3 unknown  (unknown when row if from test data)
				- ticket_same_ticket_count = 3
				- ticket_same_ticket_survived_quotient = 0.5 (unknown is 50/50 chance)	
	"""
	#generate look up table for tickets and their count and if how many survived in that group
	ticket_count_look_up = {}
	for ticket in dataframe["ticket"]:
		if ticket in ticket_count_look_up:
			group_size = ticket_count_look_up[ticket]
			group_size += 1
		else:
			group_size = 1
		ticket_count_look_up[ticket] = group_size

	#generate feature list "ticket_same_ticket_count" with use of lookup table
	ticket_same_ticket_count = []
	for ticket in df_tofill["ticket"]:
		ticket_same_ticket_count.append(ticket_count_look_up[ticket])
	df_tofill['ticket_same_ticket_count'] = ticket_same_ticket_count
	# stackoverflow says... #dx = dataframe.groupby(['ticket']).size().reset_index().rename(columns={0:'ticket_count'})
	# would have done the trick, too in one line  :-/
	
	#generate lookup table for quotient per ticket using a dictionary with a class
	tickets_and_its_survivors = {}	
	for index,row in dataframe.iterrows():
		ticket_to_test = row["ticket"]
		if ticket_to_test not in tickets_and_its_survivors:
			tickets_and_its_survivors[ticket_to_test] = SurvivalQuotientPerTicketNr()		
		if (pd.isnull(row["survived"])):
			tickets_and_its_survivors[ticket_to_test].inc_unknown() #unknown
		elif row["survived"]==1:
			tickets_and_its_survivors[ticket_to_test].inc_survived() #survived
		else:
			tickets_and_its_survivors[ticket_to_test].inc_dead()  #dead	
#		tempdict=tickets_and_its_survivors[ticket_to_test].__dict__
#		print str(tempdict+":"+str(tickets_and_its_survivors[ticket_to_test].is_unknown_passenger()))
		
	#generate feature list of survival quotient of each group 
	ticket_same_ticket_survived_quotient = []
	for ticket in df_tofill["ticket"]:
		ticket_same_ticket_survived_quotient.append(tickets_and_its_survivors[ticket].get_quotient())
	df_tofill['ticket_same_ticket_survived_quotient'] = ticket_same_ticket_survived_quotient	

	return df_tofill


def show_features_importance(df_train):
	from sklearn.ensemble import ExtraTreesClassifier
	clf = ExtraTreesClassifier(n_estimators=10000)
	train = df_train.drop(	'survived',axis=1)
	target = df_train["survived"]
	clf = clf.fit(train, target)

	features = pd.DataFrame()
	features['feature'] = train.columns
	features['importance'] = clf.feature_importances_

	print features.sort(['importance'],ascending=False)

"""
e.g.

                                 feature  importance
28                           title_Lady.    0.000000
20                            title_Don.    0.000000
19                       title_Jonkheer.    0.000000
26                            title_Mme.    0.000004
24                   title_the Countess.    0.000010
37                                deck_t    0.000015
27                           title_Mlle.    0.000016
21                            title_Sir.    0.000039
11                             title_Ms.    0.000067
25                           title_Dona.    0.000084
18                          title_Major.    0.000094
17                            title_Col.    0.000127
16                           title_Capt.    0.000162
22                             title_Dr.    0.000264
36                                deck_g    0.000272
35                                deck_f    0.000280
30                                deck_a    0.000389
23                            title_Rev.    0.000926
34                                deck_e    0.000983
33                                deck_d    0.001049
41                            embarked_Q    0.001216
32                                deck_c    0.001346
15                         title_Master.    0.001685
31                                deck_b    0.002378
42                            embarked_S    0.002654
2                                  parch    0.002923
40                            embarked_C    0.003896
38                                 cabNr    0.004168
1                                  sibsp    0.004620
6                             FamilySize    0.005066
5                                agefill    0.005489
43              ticket_same_ticket_count    0.007084
7                              Age*Class    0.007282
3                                   fare    0.007652
29                  surname_letter_count    0.010512
10                               foundid    0.011197
39                        cabin_has_info    0.012228
9                              foundbody    0.012328
0                                 pclass    0.015429
14                           title_Miss.    0.016493
13                            title_Mrs.    0.021391
12                             title_Mr.    0.047580
4                                 gender    0.063024
44  ticket_same_ticket_survived_quotient    0.334836
8                               lifeboat    0.392741
"""

def generate_features(methodToRun, train_data, test_data):
	train_data = methodToRun(train_data)
	test_data = methodToRun(test_data)
	return train_data,test_data


def get_prediction(df_train,df_test ):

	df_train_and_test = df_train.append(df_test)

	# Moving the survived column in the test data to the 2nd place for random forest
	df_train = switch_colums(df_train, "pclass", "survived")
	
	# -------------
	# DATA EXPLORING
	# -------------
	# ....
	# print df_test[0:10]
	
	# -------------
	# DATA CLEANING
	# -------------
	# --> Prepare data 'sex'
	# Turning the female/male into 0/1 and write into new column "gender"
	df_train, df_test = generate_features(get_sex, df_train, df_test)
	df_train_and_test = get_sex(df_train_and_test)
	
	# --> Prepare data 'age'
	# Fills the empy ages values with a median age value corresponding to class and gender
	# and write into new column "agefill"
	# using not only the train data but the test data, too
	df_train = get_age(df_train, df_train_and_test)
	df_test  = get_age(df_test, df_train_and_test)
	
	# --> Get feature familiy size by summing up sibles with parches
	df_train["FamilySize"] = df_train["sibsp"] + df_train["parch"]
	df_test["FamilySize"] = df_test["sibsp"] + df_test["parch"]
	
	# --> Get feature age * pclass
	df_train["Age*Class"] = df_train.agefill * df_train.pclass
	df_test["Age*Class"] = df_test.agefill * df_test.pclass
	
	# --> Include Lifeboat Infos
	df_train, df_test = generate_features(get_boat, df_train, df_test)
	
	# --> Include Body Infos
	df_train, df_test = generate_features(get_body, df_train, df_test)
	
	# --> Include Id Infos
	df_train, df_test = generate_features(get_id, df_train, df_test)
	
	# --> Include title Infos
	df_train, df_test = generate_features(get_title, df_train, df_test)
	
	# --> Include Cabin and Deck Infos
	df_train, df_test = generate_features(get_cabin, df_train, df_test)
	
	# --> Include Embarked Infos
	df_train, df_test = generate_features(get_embarked, df_train, df_test)

#REMOVED BECAUSE IT MAKES SCORE WORSE	
	# --> Include ticket number survival quotient Infos
#	df_train = get_ticket(df_train_and_test, df_train)
#	df_test = get_ticket(df_train_and_test, df_test)
	
	# -------------
	# FINAL PREP FOR MA_LEARN
	# -------------
	# Synchronize the ID's of the testdata with the original test data ID's
	test_ids = df_test.id.values

	# Get only those columns that are not numeric yet
	# print df_train.dtypes
	# print df_train.dtypes[df_train.dtypes.map(lambda x: x=='object')]
	# Drop the columns which will not be used further
	df_train = df_train.drop(
		["id","age", "name", "surname", "sex", "body", "ticket", "cabin", "embarked", "boat", "home.dest"], axis=1)
	df_test = df_test.drop(
		["id","age", "name", "surname", "sex", "body", "ticket", "cabin", "embarked", "boat", "home.dest"], axis=1)
	
	# Remove the features that have a very low importance
	#df_train = df_train.drop(	["title_Lady.","title_Jonkheer.", "title_Don.", "title_Mlle.", "title_Mme.", "title_the Countess.", "deck_t", "title_Dona.", "title_Sir.", "title_Ms.", "title_Major.","title_Col.","title_Capt.","deck_g","deck_f","deck_a","title_Rev.","deck_e","deck_d","embarked_Q","deck_c","title_Master."], axis=1)
	#df_test = df_test.drop(	["title_Lady.","title_Jonkheer.", "title_Don.", "title_Mlle.", "title_Mme.", "title_the Countess.", "deck_t", "title_Dona.", "title_Sir.", "title_Ms.", "title_Major.","title_Col.","title_Capt.","deck_g","deck_f","deck_a","title_Rev.","deck_e","deck_d","embarked_Q","deck_c","title_Master."], axis=1)
	#df_train = df_train.drop(	["title_Lady.","title_Jonkheer.", "title_Don.", "title_Mlle.", "title_Mme.", "title_the Countess.", "deck_t", "title_Dona.", "title_Sir.", "title_Ms.", "title_Major.","title_Col."], axis=1)
	#df_test = df_test.drop(	["title_Lady.","title_Jonkheer.", "title_Don.", "title_Mlle.", "title_Mme.", "title_the Countess.", "deck_t", "title_Dona.", "title_Sir.", "title_Ms.", "title_Major.","title_Col."], axis=1)

	#df_train = df_train.drop(	["ticket_same_ticket_survived_quotient"], axis=1)
	#df_test = df_test.drop(	["ticket_same_ticket_survived_quotient"], axis=1)

	
	# ToDo: find & convert the NaN values (1 value)
	df_train = df_train.dropna()
	df_test = df_test.dropna()
	#print df_train[['cabin', 'deck_a', 'deck_b', 'deck_c', 'deck_d', 'deck_e', 'deck_f', 'deck_g', 'deck_t', 'cabNr']]
	
	# now we should have a clean table --> convert back to numpy for ma_learning
	train_data = df_train.values
	test_data = df_test.values
	
	# -------------
	# RANDOM FOREST
	# -------------
	# Create the random forest object which will include all the parameters
	# for the fit
	forest = RandomForestClassifier(n_estimators=1000, max_features="auto")
	
	#print train_data[0::, 1::]
	
	# Fit the training data to the Survived labels and create the decision trees
	# First Argument: 	Training data used to predict --> train_data[0::, 1::]
	# Second Argument: 	Labels --> train_data[0::, 0]
	forest = forest.fit(train_data[0::, 1::], train_data[0::, 0])
	
	# Take the same decision trees and run it on the test data
	# This will output an array with the lengh equal to the number of passengers in test_data and a predict if they
	# have survived or not
	output = forest.predict(test_data).astype(np.int)
	
	# Match the predictions with the passenger ID of the test data and write the data into a csv file
	rmfile_if_exist(output_file)
	write_output_file(output_file, test_ids, output)
	
	#print "Prediction done with "+str(df_test.shape[1])+" features"
	if not use_internal_scoring:
		show_features_importance(df_train)
	return output

def main():
	# -------------
	# DATA READING
	# -------------
	# Read the training data
	df_train = pd.read_csv(training_file, header=0, sep=';')
	df_test = pd.read_csv(test_file, header=0, sep=';')
	
	# overrides the real test data and uses only a subset of the train data
	if use_internal_scoring:
		#split the train data to get a known test set
		df_train_temp = df_train.reindex(np.random.permutation(df_train.index))
		split_count = int(len(df_train_temp) * 0.7)
		df_train = df_train_temp[:split_count] #!!
		df_test = df_train_temp[split_count+1:]  #!!
		known_survived_from_splitting = df_test["survived"].tolist()
		df_test = df_test.drop('survived',axis=1)
			
	output = get_prediction(df_train,df_test)
	
	# If wished: the predicted result will be compared with the temporary stored real values
	if use_internal_scoring:
		i = 0
		wrong_count = 0
		for prediction in output:
			if prediction!=known_survived_from_splitting[i]:
				wrong_count += 1
			i += 1
		score = (i-wrong_count)*100.0/i
		print str(score)+"%"
		return score
	else:
		return 1

use_internal_scoring = False
if use_internal_scoring:
	range_count = 100
else:
	range_count = 0

scores = []
for _ in range(range_count):
	score = main()
	scores.append(score)

print "Score AVG: "+str(sum(scores)/float(len(scores)))+"% and MEDIAN: "+str(np.median(np.array(scores)))+"% with RANGE "+str(min(scores))+"% - "+str(max(scores))+"%"
	