# -*- coding: utf-8 -*-
"""
Created on 06 Nov 06.11.16 09:30 2016 

@author: trafrro

Description:

"""
import csv
import os
import numpy as np
from scipy import stats
from sklearn.ensemble import RandomForestClassifier

# Definitions
output_file = "prediction_rog_2.csv"

def open_csv(filename):
	""" reads the csv file and returns a numpy array"""
	# Open up the CSV file into a Python object
	with open(filename, 'rb') as f:
		csv_file_object = csv.reader(f, delimiter=';')
		header = csv_file_object.next() #next() skips the fifirst line holding the column headers
		data=[]
		for row in csv_file_object:
			#Run through each row in the CSV, add it to the data variable
			data.append(row)
	# Then convert from a list to an array
	# (Be aware that each item is currently a string in this format)
	return np.array(data)

def rmfile_if_exist(filename):
	""" checks if a file exists and removes it"""
	if os.path.exists(filename):
		try:
			os.remove(filename)
		except OSError:
			pass

def get_sex(orig_data, orig_column, data, column):
	# SEX: Prepare data 'sex' and store it in train_data
	# First: get the most frequent gender
	gender_data = orig_data[0::, orig_column]
	num_female = sum(gender_data == 'female')
	num_male = sum(gender_data == 'male')
	# Set the most frequent gender (female = 0, male = 1) -> only used if no infos available
	most_freq_gender = 0 if num_female >= num_male else 1
	# Second: store gender data in train_data
	for i, sex in enumerate(gender_data):
		if sex == '':
			data[i, column] = most_freq_gender  # Most freq. gender is used if 'sex' is undefined
		if sex == 'female':
			data[i, column] = 0
		if sex == 'male':
			data[i, column] = 1
	return data


def get_age(orig_data, orig_column, data, column):
	# AGE: Prepare data 'age' and store it in train_data
	# First: get the median age
	# Convert 'age' to float, empty values to 0
	age_data = [0 if age == '' else float(age) for age in orig_data[0::, orig_column]]
	median_age = stats.nanmedian(age_data)
	# Second: store age data in train_data
	# Alternative: train_data[0::, 5] = [median_age if age == 0 else age for age in age_data]
	for i, age in enumerate(age_data):
		if age == 0:
			data[i, column] = median_age  # Most freq. age is used if 'age' is undefined
		else:
			data[i, column] = age
	return data


def get_fare(orig_data, orig_column, data, column):
	# FARE: Prepare data 'fare' and store it in train_data
	# First: get the 'fare' and 'pclass' data
	# Convert 'fare' to float, empty values to 0
	fare_data = [0 if fare == '' else float(fare) for fare in orig_data[0::, orig_column]]
	fare_data = np.array(fare_data)  # Convert from a list to an array
	pclass_data = data[0::, 1]  # Get the 'pclass' values from train_data
	pclass_data_unique = list(enumerate(np.unique(pclass_data)))  # Get the unique 'pclass' values
	# Second: replace fares with value 0 with the median fare of the corresponding 'pclass'
	for i, unique_pclass in pclass_data_unique:
		# Get array of fares corresponding to the current pclass
		pclass_fare = fare_data[pclass_data == unique_pclass]
		# Calculate the median of the previously received fares
		median_fare = stats.nanmedian(pclass_fare)
		pclass_fare[pclass_fare == 0] = median_fare  # Replace fares with value 0 with median fare
		fare_data[pclass_data == unique_pclass] = pclass_fare
	# Third: store fare data in train_data
	data[0::, column] = fare_data[0::]
	return data


def get_embarked(orig_data, orig_column, data, column):
	# EMBARKED: Prepare data 'embarked' and store it in train_data
	# First: get the most common 'embarked' value
	embarked_data = list(orig_data[0::, orig_column])
	mc_embarked = max(set(embarked_data), key=embarked_data.count)
	# Second: replace empty entries with the most common 'embarked' value
	embarked_data = [mc_embarked if embarked == '' else embarked for embarked in embarked_data]
	embarked_data = np.array(embarked_data)
	# Third: convert all 'embarked' values to int
	# Get the unique 'embarked' values
	embarked_data_unique = list(enumerate(np.unique(embarked_data)))
	for i, unique_embarked in embarked_data_unique:
		embarked_data[embarked_data == unique_embarked] = i
	# Fourth: store embarked data in train_data
	data[0::, column] = embarked_data[0::].astype(np.float)
	return data

# Open and read Train data
orig_train_data = open_csv('titanic3_train.csv')

# COLUMN CONTENT 	ORIG.INDEX
# id 				0
# pclass 			1
# survived 			2
# name 				3
# surname 			4
# sex 				5
# age 				6
# sibsp 			7
# parch 			8
# ticket			9
# fare 				10
# cabin 			11
# embarked 			12
# boat 				13
# body 				14
# home.dest 		15

# Prepare the data structure used for training
# DATA USED FOR TRAINING 	ORIG.INDEX 		TRAIN.INDEX
# survived 					2 				0
# pclass 					1				1
# sibsp 					7 				2
# parch 					8 				3
# sex 						5 				4
# age 						6 				5
# fare 						10 				6
# embarked 					12 				7

rows = len(orig_train_data[0::, 0]) # Number of rows in the training data
cols = 8 # Number of columns in the training data
train_data = np.zeros((rows, cols)) # Array to store the data used for training

# SURVIVED: Store data 'survived' in train_data
train_data[0::, 0] = orig_train_data[0::, 2].astype(np.float)

# PCLASS: Store data 'pclass' in train_data
train_data[0::, 1] = orig_train_data[0::, 1].astype(np.float)

# SIBSP: Store data 'sibsp' in train_data
train_data[0::, 2] = orig_train_data[0::, 7].astype(np.float)

# PARCH: Store data 'parch' in train_data
train_data[0::, 3] = orig_train_data[0::, 8].astype(np.float)

# SEX: Prepare data 'sex' and store it in train_data
train_data = get_sex(orig_train_data, 5, train_data, 4)

# AGE: Prepare data 'age' and store it in train_data
train_data = get_age(orig_train_data, 6, train_data, 5)

# FARE: Prepare data 'fare' and store it in train_data
train_data = get_fare(orig_train_data, 10, train_data, 6)

# EMBARKED: Prepare data 'embarked' and store it in train_data
train_data = get_embarked(orig_train_data, 12, train_data, 7)

#---------------------------

# Open and read Test data
orig_test_data = open_csv('titanic3_test.csv')

# Overview of the test data
# DATA 		ORIG.INDEX
# id 		0
# pclass 	1
# name 		2
# surname 	3
# sex 		4
# age 		5
# sibsp 	6
# parch 	7
# ticket 	8
# fare 		9
# cabin 	10
# embarked 	11
# boat 		12
# body 		13
# home.dest	14

# Prepare the data structure used for testing
# DATA USED FOR TESTING 	ORIG.INDEX 		TEST.INDEX
# pclass 					1 				0
# sibsp 					6 				1
# parch 					7 				2
# sex 						4 				3
# age 						5 				4
# fare 						9 				5
# embarked 					11 				6

rows = len(orig_test_data[0::, 0]) # Number of rows in the test data
cols = 7 # Number of columns in the test data
test_data = np.zeros((rows, cols)) # Array to store the data used for testing

# PCLASS: Store data 'pclass' in test_data
test_data[0::, 0] = orig_test_data[0::, 1].astype(np.float)

# SIBSP: Store data 'sibsp' in test_data
test_data[0::, 1] = orig_test_data[0::, 6].astype(np.float)

# PARCH: Store data 'parch' in test_data
test_data[0::, 2] = orig_test_data[0::, 7].astype(np.float)

# SEX: Prepare data 'sex' and store it in test_data
test_data = get_sex(orig_test_data, 4, test_data, 3)

# AGE: Prepare data 'age' and store it in test_data
test_data = get_age(orig_test_data, 5, test_data, 4)

# FARE: Prepare data 'fare' and store it in test_data
test_data = get_fare(orig_test_data, 9, test_data, 5)

# EMBARKED: Prepare data 'embarked' and store it in test_data
test_data = get_embarked(orig_test_data, 11, test_data, 6)

# Synchronize the ID's of the testdata with the original test data ID's
test_ids = orig_test_data[0::,0]


#print train_data[0:3]
#print test_data[0:3]

#*********
# SciKit-learn --> random forest
# 	- initializing the model
# 	- fitting it to the training data
# 	- predicting new values
#
# some-model-name.fit(..)
# some-model-name.predict(..)
# some-model-name.score(..)
#*********
# Initialize the model
forest = RandomForestClassifier(n_estimators=100)

# Fitting it to the training data
# First Argument: 	Training data used to predict --> train_data[0::, 1::]
# Second Argument: 	Labels --> train_data[0::, 0]
forest = forest.fit(train_data[0::, 1::], train_data[0::, 0])

# Predicting new values
# This will output an array with the lengh equal to the number of passengers in test_data and a predict if they
# have survived or not
output = forest.predict(test_data).astype(np.int)

# Match the predictions with the passenger ID and write the data into a file
rmfile_if_exist(output_file)
predictions_file = open(output_file, 'wb')
open_file_object = csv.writer(predictions_file, delimiter=';')
open_file_object.writerow(['key', 'value'])
open_file_object.writerows(zip(test_ids, output))
predictions_file.close()
