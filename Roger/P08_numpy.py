# -*- coding: utf-8 -*-
"""
Created on 31 Oct 31.10.16 09:49 2016 

@author: trafrro

Description:
This Script reads a csv export of the Titanic passenger list.
It will generate a prediction rate of how likely a passenger would survive.
Output must be:
----
key;value
100001;0
100002;1
100003;0
…
----
key=> record ID
value=> survived, 0/1
"""

import os
import csv as csv
import numpy as np


def open_csv(filename):
	""" reads the csv file and returns a numpy array"""
	# Open up the CSV file into a Python object
	with open(filename, 'rb') as f:
		csv_file_object = csv.reader(f, delimiter=';')
		header = csv_file_object.next()  # next() skips the fifirst line holding the column headers
		data = []
		for row in csv_file_object:
			# Run through each row in the CSV, add it to the data variable
			data.append(row)
	# Then convert from a list to an array
	# (Be aware that each item is currently a string in this format)
	return np.array(data)


def rmfile_if_exist(filename):
	""" checks if a file exists and removes it"""
	if os.path.exists(filename):
		try:
			os.remove(filename)
		except OSError:
			pass


def fill_empty_fares(data):
	""" calculates mean fares per class and fills all empty fares with corresponding mean fare
		returns the data numpy
	"""
	# 1. calculate mean fare of each pclass
	fare_mean_per_pclass = []
	for i in range(len(np.unique(
			data[0::, 1]))):  # loop over all possible pclass's (i == pclass-1) -> np.unique => ['1' '2' '3'], len = 3
		fare_mean_per_pclass.append(0)  # initialize ith pclass fare mean
		cnt = 0  # number of tickets with a value in pclass i
		for j in range(len(data[0::, 10])):
			# Check for pclass (i+1), only sum the fares of the same class
			if data[j, 1].astype(np.int) == i + 1:
				try:  # try to cast the jth fare value as float
					fare_mean_per_pclass[i] += data[j, 10].astype(np.float)
					cnt += 1
				except:  # if it can't be casted to float: ignore it
					fare_mean_per_pclass[i] += 0  # just ignore empty values
		# calculate mean and overwrite the sum in fare_mean_per_pclass
		if cnt > 0: fare_mean_per_pclass[i] /= float(cnt)

	# print "Mean fare per pclass: ", fare_mean_per_pclass

	# 2. replace the empty values with the pclass mean
	for i in range(len(data[0::, 10])):
		try:
			# try to convert to flaot, if this works, all is well with row i
			test = float(data[i, 10])
		except ValueError:
			# set the mean fare for the pclass
			data[i, 10] = fare_mean_per_pclass[data[i, 1].astype(np.int) - 1]  # index: cur. pclass-1
	return data


####################
# Training Part
####################
data_train = open_csv("titanic3_train.csv")
#print data_train[0::, 2]

# --------
# Survival Rate
# --------
# The size() function counts how many elements are in
# the array and sum() (as you would expect) sums up
# the elements in the array.
number_passengers = np.size(data_train[0::, 2].astype(np.float))
number_survived = np.sum(data_train[0::, 2].astype(np.float))
proportion_survivors = number_survived / number_passengers
print number_passengers, number_survived, proportion_survivors

# --------
# Gender
# --------
# This finds where all the elements in the gender column equal “female”
women_only_stats = data_train[0::, 5] == "female"
# print women_only_stats
# This finds where all the elements do not equal female (i.e. male)
men_only_stats = data_train[0::, 5] != "female"
# print men_only_stats

# Using the index from above we select the females and males separately
women_onboard = data_train[women_only_stats, 2].astype(np.float)
men_onboard = data_train[men_only_stats, 2].astype(np.float)
# print women_onboard

# Then we find the proportions of them that survived
proportion_women_survived = np.sum(women_onboard) / np.size(women_onboard)
proportion_men_survived = np.sum(men_onboard) / np.size(men_onboard)
# and then print it out
print 'Proportion of women who survived is %s' % proportion_women_survived
print 'Proportion of men who survived is %s' % proportion_men_survived

# --------
# Fares
# --------
# fill up empty fare values with the mean of the corresponding pclass
data_train = fill_empty_fares(data_train)
# So we add a ceiling
fare_ceiling = 40
# then modify the data in the Fare column to =39, if it is greater or equal to the ceiling
data_train[data_train[0::, 10].astype(np.float) >= fare_ceiling, 10] = fare_ceiling - 1.0
fare_bracket_size = 10
number_of_price_brackets = fare_ceiling / fare_bracket_size
# But it's better practice to calculate this from the data directly
# Take the length of an array of unique values in column index 1
number_of_classes = len(np.unique(data_train[0::, 1]))
# Initialize the survival table with all zeros, this will generate 2 tables (male / female) with prize brackets per class
# [[[ 0.  0.  0.  0.]
#  [ 0.  0.  0.  0.]
#  [ 0.  0.  0.  0.]]
#
# [[ 0.  0.  0.  0.]
#  [ 0.  0.  0.  0.]
#  [ 0.  0.  0.  0.]]]
survival_table = np.zeros((2, number_of_classes, number_of_price_brackets))
# print survival_table

# Fill the survival table
# First Loop -> Find survived values for 1st class females, who paid less than 10.
for i in xrange(
		number_of_classes):  # loop through each class --> xrange does the same as range but is an object (less memory that list)
	for j in xrange(number_of_price_brackets):  # loop through each price bin
		# Which element is a female, and was ith class, was greater than this bin,
		# and less than the next bin -> give the the 3rd col (survived)
		women_only_stats = data_train[(data_train[0::, 5] == "female") \
									  & (data_train[0::, 1].astype(np.float) == i + 1) \
									  & (data_train[0:, 10].astype(np.float) >= j * fare_bracket_size) \
									  & (data_train[0:, 10].astype(np.float) < (j + 1) * fare_bracket_size) \
									, 2]
		# Which element is a male, and was ith class, was greater than this bin,
		# and less than the next bin
		men_only_stats = data_train[(data_train[0::, 5] != "female") \
									& (data_train[0::, 1].astype(np.float) == i + 1) \
									& (data_train[0:, 10].astype(np.float) >= j * fare_bracket_size) \
									& (data_train[0:, 10].astype(np.float) < (j + 1) * fare_bracket_size) \
									, 2]
		# record the found values to the survival table with the mean value of all found values to get the chance in % of survival
		survival_table[0, i, j] = np.mean(women_only_stats.astype(np.float))
		survival_table[1, i, j] = np.mean(men_only_stats.astype(np.float))
		# set the nan values to 0
		survival_table[survival_table != survival_table] = 0.

#print survival_table
# update the survival table with the assumption of >0.5 will survive (survival==1) and >0.5 will not survive (survival==0)
survival_table[survival_table < 0.5] = 0
survival_table[survival_table >= 0.5] = 1
print survival_table

####################
# Submission Part
####################

output_filename = "prediction_rog.csv"

# read in the titanic3_test.csv file and skip the header line:
test_file = open('titanic3_test.csv', 'rb')
test_file_object = csv.reader(test_file, delimiter=';')
header = test_file_object.next()

# open new file in csv format
rmfile_if_exist(output_filename)
prediction_file = open(output_filename, "wb")
prediction_file_object = csv.writer(prediction_file, delimiter=';')

# Write header
prediction_file_object.writerow(["key", "value"])

# # Prediction based on gender
# for row in test_file_object:
# 	if row[4] == 'female':
# 		prediction_file_object.writerow([row[0],'1']) # predict 1
# 	else: #it is male
# 		prediction_file_object.writerow([row[0],'0']) # predict 0

# Prediction based on gender/class/fare and survival_table
# We are going to loop through each passenger in the test set
for row in test_file_object:
	# For each passenger we loop through each price bin
	for j in xrange(number_of_price_brackets):
		try:  # Some passengers have no fare data so try to make…
			row[9] = float(row[9])  # a float
		except:  # If fails: no data, so…
			bin_fare = 3 - float(row[1])  # assume the fare bin according to pclass
			break
		if row[9] > fare_ceiling:  # If there is data see if it is greater than fare ceiling
			bin_fare = number_of_price_brackets - 1  # If so set to highest bin
			break
		# If passed these tests then loop through each bin
		if row[9] >= j * fare_bracket_size and row[9] < (j + 1) * fare_bracket_size:
			bin_fare = j  # If passed these tests then assign index
			break

		# Write line depending on gender / class and fare by checking against the survival_table
		if row[4] == 'female':  # If the passenger is female
			# Write "key", "value" format
			# key 	= row[0]
			# value	=  "%d" % int(survival_table[0, float(row[1]) - 1, bin_fare]) => female / class / fare
			# prediction_file_object.writerow([row[0], "%d" % int(survival_table[0, float(row[1]) - 1, bin_fare])])
			prediction_file_object.writerow([int(row[0]), int(survival_table[0, float(row[1]) - 1, bin_fare])])
		else:  # passenger is male
			prediction_file_object.writerow([int(row[0]), int(survival_table[1, float(row[1]) - 1, bin_fare])])

test_file.close()
prediction_file.close()
