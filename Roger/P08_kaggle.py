# -*- coding: utf-8 -*-
"""
Created on 06 Nov 06.11.16 06:58 2016 

@author: trafrro

Description:

"""
import pandas as pd
import numpy as np
import pylab as P
# Import the random forest package
from sklearn.ensemble import RandomForestClassifier

#-------------
# DATA READING
#-------------
# For .read_csv, always use header=0 when you know row 0 is the header row
df = pd.read_csv('titanic3_train.csv', header=0, sep=';')

# Moving the survived column to the 2nd place
columns = df.columns.tolist()
a, b = columns.index('pclass'), columns.index('survived')
columns[b], columns[a] = columns[a], columns[b]
df = df[columns]

#-------------
# DATA EXPLORING
#-------------
# To explore the Data, see those useful commands
#print df_train.dtypes
#print df_train.info()
#print df_train.describe()

# print the first 10 rows of column age
#print df_train['age'][0:10]
#print df_train.age[0:10]

#print "\nMean age value:", df_train['age'].mean()
#print df_train.age.mean()
#print df_train.age.median()

# Print only certain columns
#print df_train[["sex", "pclass", "age"]]

# Print only certain columns with a where clause (older than 60)
#print df_train[df_train.age > 60][["sex", "pclass", "age", "survived"]]

# Filter for null values
#print df_train[df_train.age.isnull()][["sex", "pclass", "age"]]

# Print count of men per pclass
# for i in range(1,4):
# 	print i, len(df_train[ (df_train.sex == "male") & (df_train.pclass == i) ])

# Plotting the histogramm of age
#df_train.age.hist()
#df_train.age.dropna().hist(bins=16, range=(0,80), alpha=0.3)
#P.show()

#-------------
# DATA CLEANING
#-------------
# adding a new column with default value 4
#df_train['gender'] = 4
#print df_train.head(5)

# Turning the first character of male/female into Capital M/F in the new column
# lambda is an built-in function of python for generating anonymous function in the moment at runtime
#df_train['gender'] = df_train.sex.map( lambda x: x[0].upper() )
#print df_train.head(5)

# Turning the female/male into 0/1 for further analysis
df['gender'] = df.sex.map( {'female':0, 'male':1} ).astype(int)
#print df_train.head(5)

# --> FillNull on Age
# to fill the null values in age, lets create a new reference table
median_ages = np.zeros((2,3))
for i in range(0, 2): # for each dimension in array
	for j in range(0, 3): # for each value in the array
		#print "Gender:", i, "Class:", j+1
		#print df_train[ (df_train.gender == i) & (df_train.pclass == j+1) ][["sex", "pclass", "age"]]
		#print df_train[(df_train.gender == i) & (df_train.pclass == j + 1)]["age"].dropna().median()
		median_ages[i,j] = df[ (df.gender == i) & (df.pclass == j+1) ]["age"].dropna().median()

#print median_ages

# copy original age column to fill the null values and still see the original
df['agefill'] = df['age']

# print null values in age column
#print df_train[ df_train.age.isnull() ][["gender", "pclass", "age", "agefill"]].head(10)

# fill null values with values from our median_ages table
for i in range(0, 2): # for female(0) and male(1)
	for j in range(0, 3): # for each pclass 1-3
		df.loc[(df.age.isnull()) & (df.gender == i) & (df.pclass == j+1), "agefill"] = median_ages[i,j]
		#print df_train.loc[(df_train.age.isnull()) & (df_train.gender == i) & (df_train.pclass == j+1)][["sex", "pclass", "age", "agefill"]]

#print df_train[df_train.age.isnull()][["sex", "pclass", "age", "agefill"]].head(10)

# Flag the collumns where the age was originally null in a new column
df['ageisnull'] = pd.isnull(df.age).astype(int)
#print df_train[df_train.age.isnull()][["sex", "pclass", "age", "agefill", "ageisnull"]].head(10)

#print df_train.describe()

# --> GetFamilySize
df["FamilySize"] = df["sibsp"] + df["parch"]
df["Age*Class"] = df.agefill * df.pclass
#df_train["Age*Class"].hist()
#P.show()

#-------------
# FINAL PREP FOR MA_LEARN
#-------------
# Get only those columns that are not numeric yet
#print df_train.dtypes[df_train.dtypes.map(lambda x: x=='object')]
# Drop the columns which will not be used further
df = df.drop(["id", "age", "name", "surname", "sex", "body", "ticket", "cabin", "embarked", "boat", "home.dest"], axis=1)

#print df_train.dtypes

#print df_train.describe()
# To do: find & convert the NAN values (1 value)
df = df.dropna()
print df.describe()

# now we should have a clean table --> convert back to numpy for ma_learning
#print df_train.values
train_data = df.values
print train_data

#-------------
# RANDOM FOREST
#-------------
# Create the random forest object which will include all the parameters
# for the fit
forest = RandomForestClassifier(n_estimators = 100)

# Fit the training data to the Survived labels and create the decision trees
forest = forest.fit(train_data[0::,1::],train_data[0::,0])

# Take the same decision trees and run it on the test data
#output = forest.predict(test_data)