# -*- coding: utf-8 -*-
"""
Created on 06 Nov 06.11.16 13:21 2016 

@author: trafrro

Description:
This Script reads a csv export of the Titanic passenger list.
It will read a training file (survival is known) and generates a prediction rate
of how likely a passenger would survive out of a test file (survival is unknown).
Output must be:
----
key;value
100001;0
100002;1
100003;0
"""

import os
import csv
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier

# Definitions
output_file = "prediction_rog_5.csv"
training_file = "titanic3_train.csv"
test_file = "titanic3_test.csv"


def rmfile_if_exist(filename):
	"""
	checks if a file exists and removes it
	:param filename: string
	:return:
	"""
	if os.path.exists(filename):
		try:
			os.remove(filename)
		except OSError:
			pass


def write_output_file(filename, key, value):
	""" Match the predictions with the passenger ID of the test data and write the data into a csv file
		Format:
		key;value
		100001;0
		100002;1
		100003;0
	:param filename: string
	:param key: list
	:param value: list
	:return:
	"""
	predictions_file = open(filename, 'wb')
	open_file_object = csv.writer(predictions_file, delimiter=';')
	open_file_object.writerow(['key', 'value'])
	open_file_object.writerows(zip(key, value))
	predictions_file.close()


def get_median_ages(dataframe, count_gender, count_class):
	"""
	Creates a new numpy array which contains the median ages based on:
		- gender
		- class
	:param dataframe: pandas.DataFrame
	:param count_gender: int
	:param count_class: int
	:return: numpy.array
	"""
	median_ages = np.zeros((count_gender, count_class))
	for i in range(0, count_gender):  # for each gender
		for j in range(0, count_class):  # for each class in gender
			# print "Gender:", i, "Class:", j+1
			# print df_train[ (df_train.gender == i) & (df_train.pclass == j+1) ][["sex", "pclass", "age"]]
			# print df_train[(df_train.gender == i) & (df_train.pclass == j + 1)]["age"].dropna().median()
			median_ages[i, j] = dataframe[(dataframe.gender == i) & (dataframe.pclass == j + 1)][
				"age"].dropna().median()
	return median_ages


def switch_colums(dataframe, column1, column2):
	"""
	will switch two columns of a given DataFrame
	:param dataframe: pandas.DataFrame
	:param column1: string
	:param column2: string
	:return: dataframe
	"""
	columns = dataframe.columns.tolist()
	a, b = columns.index(column1), columns.index(column2)
	columns[b], columns[a] = columns[a], columns[b]
	return dataframe[columns]


def get_age(dataframe):
	"""
	will receive a dataframe with empty values in aging column
		1: generates a median ages reference array
		2: will fill the empty ages value with the corresponding median ages value
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	# first, lets create a new reference table with median ages per gender and class
	count_class = len(dataframe["pclass"].unique())
	count_gender = len(dataframe["gender"].unique())
	median_ages = get_median_ages(dataframe, count_gender, count_class)
	# print median_ages

	# copy original age column to fill the null values and still see the original
	dataframe['agefill'] = dataframe['age']

	# fill null values with values from our median_ages table
	for i in range(0, count_gender):  # for female(0) and male(1)
		for j in range(0, count_class):  # for each pclass 1-3
			dataframe.loc[(dataframe.age.isnull()) & (dataframe.gender == i) & (dataframe.pclass == j + 1), "agefill"] = \
				median_ages[i, j]
		# print dataframe.loc[(dataframe.age.isnull()) & (dataframe.gender == i) & (dataframe.pclass == j+1)][["sex", "pclass", "age", "agefill"]]
	return dataframe


def get_sex(dataframe):
	"""
	Turning the female/male into 0/1 for further analysis
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	dataframe['gender'] = dataframe.sex.map({'female': 0, 'male': 1}).astype(int)
	return dataframe


def get_boat(dataframe):
	"""
	Reads the boat infos and fills a new column with 0/1 (found on lifeboat no/yes)
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	# create new column and copy the boat infos
	dataframe['lifeboat'] = dataframe['boat']

	# fill empty values with 0
	dataframe.loc[(dataframe['boat'].isnull()), 'lifeboat'] = 0
	#print dataframe.loc[(dataframe['boat'].isnull()), 'lifeboat']

	# fill all cells containing a value with 1
	dataframe.loc[(dataframe['boat'].notnull()), 'lifeboat'] = 1
	#print dataframe.loc[(dataframe['boat'].notnull())]

	dataframe['lifeboat'] = dataframe['lifeboat'].astype(int)
	# check result
	#print dataframe[0:100][['id', 'survived', 'boat', 'lifeboat']]
	return dataframe


def get_body(dataframe):
	"""
	Reads the body infos and fills a new column with 0/1 (found body no/yes)
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	# create new column and copy the boat infos
	dataframe['foundbody'] = dataframe['body']

	# fill empty values with 0
	dataframe.loc[(dataframe['body'].isnull()), 'foundbody'] = 0
	#print dataframe.loc[(dataframe['boat'].isnull()), 'lifeboat']

	# fill all cells containing a value with 1
	dataframe.loc[(dataframe['body'].notnull()), 'foundbody'] = 1
	#print dataframe.loc[(dataframe['boat'].notnull())]

	dataframe['foundbody'] = dataframe['foundbody'].astype(int)
	# check result
	#print dataframe[0:100][['id', 'survived', 'boat', 'lifeboat']]
	return dataframe


def get_cabin(dataframe):
	"""
	Separate Deck and Cabin information into separate columns and fill empty values
	:param dataframe: pandas.DataFrame
	:return: dataframe
	"""
	# create tuple and dataframe corresponding deck columns
	deck = ("a", "b", "c", "d", "e", "f", "g", "t")
	for d in deck:
		col_name = "deck_" + d
		dataframe[col_name] = 0		# create new deck colums
	dataframe['cabNr'] = 0 			# create new column with cabin number

	# Iterate through the DataFrame an generate the deck features
	for index, row in dataframe.iterrows():
		# ignore empty cabin values
		if (pd.isnull(row['cabin'])):
			pass
		else:
			# make infos lowercase and split into list
			cabin_list = str(row['cabin']).lower().split(" ")
			# deal with values with more than one entry
			if len(cabin_list) > 1:
				cab_nr = []
				# take each cabin in list
				for cabin in cabin_list:
					# ignore single cabin values
					if not len(cabin) > 1:
						pass
					# go through each deck value
					for d in deck:
						col_name = "deck_" + d
						# write flag into deck
						if d in cabin:
							#print cabin_list
							dataframe.loc[index, col_name] = 1
					# get cabin number, read as int since we want to calculate the mean result
					if cabin[1::]:
						cab_nr.append(int(cabin[1::]))
				# calculate mean and write into cabin number column
				dataframe.loc[index, 'cabNr'] = np.array(cab_nr).mean()
			else:
				# write deck flag
				for d in deck:
					col_name = "deck_" + d
					if d in cabin_list[0]:
						dataframe.loc[index, col_name] = 1
				# write cabin number if we have one
				cabin_number = cabin_list[0][1::]
				if cabin_list[0][1::]:
					dataframe.loc[index, 'cabNr'] = cabin_list[0][1::]
	dataframe['cabNr'] = dataframe['cabNr'].astype(int)
	return dataframe


# -------------
# DATA READING
# -------------
# Read the training data
df_train = pd.read_csv(training_file, header=0, sep=';')
df_test = pd.read_csv(test_file, header=0, sep=';')

# Moving the survived column in the test data to the 2nd place for random forest
df_train = switch_colums(df_train, "pclass", "survived")

# -------------
# DATA EXPLORING
# -------------
# ....
#print df_test[0:10]

# -------------
# DATA CLEANING
# -------------
# --> Prepare data 'sex'
# Turning the female/male into 0/1 and write into new column "gender"
df_train = get_sex(df_train)
df_test = get_sex(df_test)

# --> Prepare data 'age'
# Fills the empy ages values with a median age value corresponding to class and gender
# and write into new column "agefill"
df_train = get_age(df_train)
df_test = get_age(df_test)

# --> Get feature familiy size by multiplicate sibles with parches
df_train["FamilySize"] = df_train["sibsp"] + df_train["parch"]
df_test["FamilySize"] = df_test["sibsp"] + df_test["parch"]

# --> Get feature age * pclass
df_train["Age*Class"] = df_train.agefill * df_train.pclass
df_test["Age*Class"] = df_test.agefill * df_train.pclass

# --> Include Lifeboat Infos
df_train = get_boat(df_train)
df_test = get_boat(df_test)

# --> Include Body Infos
df_train = get_body(df_train)
df_test = get_body(df_test)

# --> Include Cabin and Deck Infos
df_train = get_cabin(df_train)
df_test = get_cabin(df_test)

# -------------
# FINAL PREP FOR MA_LEARN
# -------------
# Synchronize the ID's of the testdata with the original test data ID's
test_ids = df_test.id.values

# Get only those columns that are not numeric yet
#print df_train.dtypes
#print df_train.dtypes[df_train.dtypes.map(lambda x: x=='object')]
# Drop the columns which will not be used further
df_train = df_train.drop(
	["id", "age", "name", "surname", "sex", "body", "ticket", "cabin", "embarked", "boat", "home.dest"], axis=1)
df_test = df_test.drop(
	["id", "age", "name", "surname", "sex", "body", "ticket", "cabin", "embarked", "boat", "home.dest"], axis=1)

# ToDo: find & convert the NaN values (1 value)
df_train = df_train.dropna()
df_test = df_test.dropna()
#print df_train.describe()

# now we should have a clean table --> convert back to numpy for ma_learning
train_data = df_train.values
test_data = df_test.values

# -------------
# RANDOM FOREST
# -------------
# Create the random forest object which will include all the parameters
# for the fit
forest = RandomForestClassifier(n_estimators=100)

# Fit the training data to the Survived labels and create the decision trees
# First Argument: 	Training data used to predict --> train_data[0::, 1::]
# Second Argument: 	Labels --> train_data[0::, 0]
forest = forest.fit(train_data[0::, 1::], train_data[0::, 0])

# Take the same decision trees and run it on the test data
# This will output an array with the lengh equal to the number of passengers in test_data and a predict if they
# have survived or not
output = forest.predict(test_data).astype(np.int)

# Write Output into file
rmfile_if_exist(output_file)
write_output_file(output_file, test_ids, output)