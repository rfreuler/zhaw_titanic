# -*- coding: utf-8 -*-
"""
Created on 06 Nov 06.11.16 13:21 2016 

@author: trafrro

Description:
This Script reads a csv export of the Titanic passenger list.
It will read a training file (survival is known) and generates a prediction rate
of how likely a passenger would survive out of a test file (survival is unknown).
Output must be:
----
key;value
100001;0
100002;1
100003;0
"""

import os
import csv
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier

# Definitions
output_file = "mojado_randomforest_31_features.csv"
training_file = "titanic3_train.csv"
test_file = "titanic3_test.csv"


def rmfile_if_exist(filename):
    """ checks if a file exists and removes it
	input:
		filename: str
	return:
		None
	"""
    if os.path.exists(filename):
        try:
            os.remove(filename)
        except OSError:
            pass


def get_median_ages(dataframe, count_gender, count_class):
    """ Creates a new numpy array which contains the median ages based on:
	- gender
	- class

	input:
		dataframe: pandas.DataFrame
		count_gender: int
		count_class: int
	return:
		median_ages: numpy.array
	"""
    median_ages = np.zeros((count_gender, count_class))
    for i in range(0, count_gender):  # for each gender
        for j in range(0, count_class):  # for each class in gender
            # print "Gender:", i, "Class:", j+1
            # print df_train[ (df_train.gender == i) & (df_train.pclass == j+1) ][["sex", "pclass", "age"]]
            # print df_train[(df_train.gender == i) & (df_train.pclass == j + 1)]["age"].dropna().median()
            median_ages[i, j] = dataframe[(dataframe.gender == i) & (dataframe.pclass == j + 1)][
                "age"].dropna().median()
    return median_ages


def switch_colums(dataframe, column1, column2):
    """ will switch two columns of a given DataFrame
	input:
		dataframe: pandas.DataFrame
		column1: str
		column2: str
	return:
		dataframe: pandas.DataFrame
	"""
    columns = dataframe.columns.tolist()
    a, b = columns.index(column1), columns.index(column2)
    columns[b], columns[a] = columns[a], columns[b]
    return dataframe[columns]


def get_age(dataframe, dataframe_train_and_test):
    """ will receive a dataframe with empty values in aging column
		1: generates a median ages reference array
		2: will fill the empty ages value with the corresponding median ages value
	input:
		dataframe: pandas.DataFrame
	return:
		dataframe: pandas.DataFrame
	"""
    # first, lets create a new reference table with median ages per gender and class
    count_class = len(dataframe_train_and_test["pclass"].unique())
    count_gender = len(dataframe_train_and_test["gender"].unique())
    median_ages = get_median_ages(dataframe_train_and_test, count_gender, count_class)
    # print median_ages

    # copy original age column to fill the null values and still see the original
    dataframe['agefill'] = dataframe['age']


    # fill null values with values from our median_ages table
    for i in range(0, count_gender):  # for female(0) and male(1)
        for j in range(0, count_class):  # for each pclass 1-3
            dataframe.loc[(dataframe.age.isnull()) & (dataframe.gender == i) & (dataframe.pclass == j + 1), "agefill"] = \
                median_ages[i, j]
        # print dataframe.loc[(dataframe.age.isnull()) & (dataframe.gender == i) & (dataframe.pclass == j+1)][["sex", "pclass", "age", "agefill"]]
    return dataframe


def get_sex(dataframe):
    """ Turning the female/male into 0/1 for further analysis
	input:
		dataframe: pandas.DataFrame
	return:
		dataframe: pandas.DataFrame
	"""
    dataframe['gender'] = dataframe.sex.map({'female': 0, 'male': 1}).astype(int)
    return dataframe


def get_boat(dataframe):
    """ Reads the boat infos and fills a new column with 0/1 (found on lifeboat no/yes)
	input:
		dataframe: pandas.DataFrame
	return:
		dataframe: pandas.DataFrame
	"""
    # create new column and copy the boat infos
    dataframe['lifeboat'] = dataframe['boat']

    # fill empty values with 0
    dataframe.loc[(dataframe['boat'].isnull()), 'lifeboat'] = 0
    # print dataframe.loc[(dataframe['boat'].isnull()), 'lifeboat']

    # fill all cells containing a value with 1
    dataframe.loc[(dataframe['boat'].notnull()), 'lifeboat'] = 1
    # print dataframe.loc[(dataframe['boat'].notnull())]

    dataframe['lifeboat'] = dataframe['lifeboat'].astype(int)
    # check result
    # print dataframe[0:100][['id', 'survived', 'boat', 'lifeboat']]
    return dataframe


def get_body(dataframe):
    """ Reads the body infos and fills a new column with 0/1 (found body no/yes)
	input:
		dataframe: pandas.DataFrame
	return:
		dataframe: pandas.DataFrame
	"""
    # create new column and copy the boat infos
    dataframe['foundbody'] = dataframe['body']

    # fill empty values with 0
    dataframe.loc[(dataframe['body'].isnull()), 'foundbody'] = 0
    # print dataframe.loc[(dataframe['boat'].isnull()), 'lifeboat']

    # fill all cells containing a value with 1
    dataframe.loc[(dataframe['body'].notnull()), 'foundbody'] = 1
    # print dataframe.loc[(dataframe['boat'].notnull())]

    dataframe['foundbody'] = dataframe['foundbody'].astype(int)
    # check result
    # print dataframe[0:100][['id', 'survived', 'boat', 'lifeboat']]
    return dataframe


def get_id(dataframe):
    """ Reads the id infos and fills a new column with the id number
	input:
		dataframe: pandas.DataFrame
	return:
		dataframe: pandas.DataFrame
	"""
    dataframe['foundid'] = dataframe['id']
    dataframe['foundid'] = dataframe['foundid'].astype(int)
    return dataframe
    
def get_title(dataframe):
    """ Reads the surname infos, extracts the titles and creates for each title a new category
	input:
		dataframe: pandas.DataFrame
	return:
		dataframe: pandas.DataFrame
	"""
    all_found_titles = ["Ms.","Mr.","Mrs.","Miss.","Master.","Capt.","Col.","Major.","Jonkheer.","Don.","Sir.","Dr.","Rev.","the Countess.","Dona.","Mme.","Mlle.","Lady."]

    for title in all_found_titles:
        titleCategoryMap = []
        for title_and_surname in dataframe["surname"]:
            if title in title_and_surname:
                titleCategoryMap.append(1)
            else:
                titleCategoryMap.append(0)
        feature_name = "title_"+title
        dataframe[feature_name] = titleCategoryMap

    return dataframe #checked output with variable explorer and excel. seems to work.


# -------------
# DATA READING
# -------------
# Read the training data
df_train = pd.read_csv(training_file, header=0, sep=';')
df_test = pd.read_csv(test_file, header=0, sep=';')

df_train_and_test = df_train.append(df_test)

# overrides the real test data and uses only a subset of the train data
use_internal_scoring = True
if use_internal_scoring:
    #split the train data to get a known test set 
    df_train_temp = df_train.reindex(np.random.permutation(df_train.index))
    split_count = int(len(df_train_temp) * 0.7)
    df_train = df_train_temp[:split_count] #!!
    df_test = df_train_temp[split_count+1:]  #!!
    known_survived_from_splitting = df_test["survived"].tolist()
    df_test = df_test.drop('survived',axis=1)

# Moving the survived column in the test data to the 2nd place for random forest
df_train = switch_colums(df_train, "pclass", "survived")

# -------------
# DATA EXPLORING
# -------------
# ....
# print df_test[0:10]

# -------------
# DATA CLEANING
# -------------
# --> Prepare data 'sex'
# Turning the female/male into 0/1 and write into new column "gender"
df_train = get_sex(df_train)
df_test = get_sex(df_test)
df_train_and_test = get_sex(df_train_and_test)

# --> Prepare data 'age'
# Fills the empy ages values with a median age value corresponding to class and gender
# and write into new column "agefill"
# using not only the train data but the test data, too
df_train = get_age(df_train, df_train_and_test)
df_test  = get_age(df_test, df_train_and_test)

# --> Get feature familiy size by multiplicate sibles with parches
df_train["FamilySize"] = df_train["sibsp"] + df_train["parch"]
df_test["FamilySize"] = df_test["sibsp"] + df_test["parch"]

# --> Get feature age * pclass
df_train["Age*Class"] = df_train.agefill * df_train.pclass
df_test["Age*Class"] = df_test.agefill * df_test.pclass

# --> Include Lifeboat Infos
df_train = get_boat(df_train)
df_test = get_boat(df_test)

# --> Include Body Infos
df_train = get_body(df_train)
df_test = get_body(df_test)

# --> Include Id Infos
df_train = get_id(df_train)
df_test = get_id(df_test)

# --> Include title Infos
df_train = get_title(df_train)
df_test = get_title(df_test)


# -------------
# FINAL PREP FOR MA_LEARN
# -------------
# Synchronize the ID's of the testdata with the original test data ID's
test_ids = df_test.id.values

# Get only those columns that are not numeric yet
# print df_train.dtypes
# print df_train.dtypes[df_train.dtypes.map(lambda x: x=='object')]
# Drop the columns which will not be used further
df_train = df_train.drop(
    ["id","age", "name", "surname", "sex", "body", "ticket", "cabin", "embarked", "boat", "home.dest"], axis=1)
df_test = df_test.drop(
    ["id","age", "name", "surname", "sex", "body", "ticket", "cabin", "embarked", "boat", "home.dest"], axis=1)

# ToDo: find & convert the NaN values (1 value)
df_train = df_train.dropna()
df_test = df_test.dropna()
print df_train.describe()

# now we should have a clean table --> convert back to numpy for ma_learning
train_data = df_train.values
test_data = df_test.values

# -------------
# RANDOM FOREST
# -------------
# Create the random forest object which will include all the parameters
# for the fit
forest = RandomForestClassifier(n_estimators=100)

print train_data[0::, 1::]

# Fit the training data to the Survived labels and create the decision trees
# First Argument: 	Training data used to predict --> train_data[0::, 1::]
# Second Argument: 	Labels --> train_data[0::, 0]
forest = forest.fit(train_data[0::, 1::], train_data[0::, 0])

# Take the same decision trees and run it on the test data
# This will output an array with the lengh equal to the number of passengers in test_data and a predict if they
# have survived or not
output = forest.predict(test_data).astype(np.int)


# If wished: the predicted result will be compared with the temporary stored real values
"""
 TODO: Unfortunately, for this small data set the fluctuation is to big. 
       Idea: Make this 100 times and take the avg score value! 
       Also, compare the values with less features and the resulted submission score to get an idea for its efficiency
"""
if use_internal_scoring:
    i = 0
    wrong_count = 0
    for prediction in output:
        if prediction!=known_survived_from_splitting[i]:
            wrong_count += 1
        i += 1
    score = (i-wrong_count)*100.0/i
    print "Prediction: "+str(score)+"%  with "+str(df_train.shape[1])+" features"

# Match the predictions with the passenger ID of the test data and write the data into a csv file
rmfile_if_exist(output_file)
predictions_file = open(output_file, 'wb')
open_file_object = csv.writer(predictions_file, delimiter=';')
open_file_object.writerow(['key', 'value'])
open_file_object.writerows(zip(test_ids, output))
predictions_file.close()
