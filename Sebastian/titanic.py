# -*- coding: utf-8 -*-

"""
links for pandas:
    https://www.analyticsvidhya.com/blog/2015/07/11-steps-perform-data-analysis-pandas-python/
    http://www.webpages.uidaho.edu/~stevel/504/Pandas%20DataFrame%20Notes.ptrain_data
    https://s3.amazonaws.com/quandl-static-content/Documents/Quandl+-+Pandas,+SciPy,+NumPy+Cheat+Sheet.ptrain_data

cookbooks:
    http://ahmedbesbes.com/how-to-score-08134-in-titanic-kaggle-challenge.html
"""

import pandas as pd
import numpy as np
from numpy import savetxt
import csv as csv
import random
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report
from sklearn.cross_validation import cross_val_score
import sklearn.metrics

#feature Importance
from sklearn import datasets
from sklearn.ensemble import ExtraTreesClassifier

test_data_path = 'titanic3_test.csv'
train_data_path = 'titanic3_train.csv'



def make_a_demo_solution_with_semi_random(test_data_path, output_data_path):
    test_file = open(test_data_path, 'rb')
    test_file_object = csv.reader(test_file, delimiter=';')
    header = test_file_object.next()
    #print header
    prediction_file = open(output_data_path, "wb")
    prediction_file_object = csv.writer(prediction_file, delimiter=';')
    prediction_file_object.writerow(["key", "value"])

    for row in test_file_object:
        rand = random.randint(0,9)
        if rand>7: #whatever
            prediction_file_object.writerow([row[0],'0']) # predict death
        else: 
            prediction_file_object.writerow([row[0],'1']) # predict survived
    
    test_file.close()
    prediction_file.close()

def make_a_submission_file(test_data_id, predicted_survived, output_data_path):

    prediction_file = open(output_data_path, "wb")
    prediction_file_object = csv.writer(prediction_file, delimiter=';')
    prediction_file_object.writerow(["key", "value"])

    index = 0;
    for row in predicted_survived:
        if row == 0:
            prediction_file_object.writerow([test_data_id[index],'0'])  # predict death
        else:
            prediction_file_object.writerow([test_data_id[index],'1'])  # predict survived
        index += 1
    prediction_file.close()


#train_data = pd.DataFrame(raw_data, columns = ['first_name', 'last_name', 'age', 'preTestScore', 'postTestScore'])
#train_data = pd.read_csv(test_data_path,sep=';',columns=['id','pclass','name','surname','sex','age','sibsp','parch','ticket','fare','cabin','embarked','boat','body','home.dest'])

train_data = pd.read_csv(train_data_path,sep=';')  # train_data = dataframe
print '_____________'
print 'train_data.head() : '
print train_data.head()  # shows header date
print '_____________'
print 'train_data.describe() :'
print train_data.describe() # show some overall statistics
print '_____________'


test_data = pd.read_csv(test_data_path,sep=';')  # train_data = dataframe
print '_____________'
print 'test_data.head() : '
print test_data.head()  # shows header date
print '_____________'
print 'test_data.describe() :'
print test_data.describe() # show some overall statistics
print '_____________'

train_and_test_data = train_data.append(test_data)
print '_____________'
print 'train_and_test_data.head() : '
print train_and_test_data.head()  # shows header date
print '_____________'
print 'train_and_test_data.describe() :'
print train_and_test_data.describe() # show some overall statistics
print '_____________'

if len(train_and_test_data['id']) > len(set(train_and_test_data['id'])):
    raise IndexError


"""
1=yes   , 0=no
no value = NaN

TODOs:
* age
- find NaN and fill in average
- find NaN and fill in average for title-category
- age can be a category, too: Make categories like 0..2years / 2..10years / 10..18years / 18..30years /...

* boat  (mostly: when not NaN then survived)
- if NaN:0, else 1  (boat_has_found_boat)

* body (=not survived)
- if NaN:0, else 1  (body_has_dead_body)

* cabin
- new feature: cabin_has_info(1/0)
- seperate between letter (A,B,C,... and number):
    - cabin_in_A
    - cabin_in_B
    - cabin_in_C
    - cabin_in_D
    - cabin_in_E
    - cabin_in_F
    - cabin_in_G
    - cabin_in_T
    and
    - cabin_number
- optional: use real Titanic blueprint layers to build cabin location categories
    - cabin_section1
    - cabin_section2
    - cabin_section3
    ...

* embarked
    - embarked_in_S
    - embarked_in_C
    - embarked_in_Q
    - embarked_in_NoInfo

* fare
    - find NaN and make it ...0?

* home.destination
    - ignore

* id
    - ignore? no

* name
    - ignore

* parch (Number of Parents/Children Aboard)
    - ok

* pclass
    - categories: pclass_in_1, pclass_in_2, pclass_in_3

* sex
    - sex: 0/1

* sibsp
    - ok

* surname
    - divide title from surname
        - categories: surname_title_mr, surname_title_master, surname_title_mrs, ...
        - new category: surname_without_title_lettercount

* ticket
    - ignore
    - find others with same number and calculate survival quotient
        e.g.unique ticket number:
                - ticket_same_ticket_count = 0
                - ticket_same_ticket_survived_quotient = 0
            3 same ticket number, 2 survived, 1 dead
                - ticket_same_ticket_count = 3
                - ticket_same_ticket_survived_quotient = 0.666
            4 same ticket number, 0 survived, 4 dead
                - ticket_same_ticket_count = 4
                - ticket_same_ticket_survived_quotient = 0

"""

#train_and_test_data.to_csv('train_and_test_data.csv', encoding='utf-8')

def check_pclass_sanity():
    for value in train_data["pclass"]:
        if 1 > value > 3:
            raise IndexError


# change sex
train_data['sex'] = train_data['sex'].map({'male':1,'female':0})
test_data['sex']  = test_data['sex'].map({'male':1,'female':0})

# change pclass
check_pclass_sanity() # are all pclass listed?


def main():
    #create and train the random forest
    #multi-core CPUs can use: rf = RandomForestClassifier(n_estimators=100, n_jobs=2)
    rf = RandomForestClassifier(n_estimators=100)
    survived = train_data['survived'].values
   # survived = survived[:, np.newaxis]
    features = train_data['sex'].values
    rf.fit(features, survived)

    testset = test_data['sex'][:, np.newaxis]
    predicted_survived = rf.predict(testset)
    make_a_submission_file(test_data['id'], predicted_survived, "mojado_randomforest_2features.csv")


#print_random_forest_trees_information()

if __name__=="__main__":  # create entry point because of multi threading python stuff
    main()








def print_random_forest_trees_information():
    from sklearn import tree
    i_tree = 0
    for tree_in_forest in forest.estimators_:
        with open('tree_' + str(i_tree) + '.txt', 'w') as my_file:
            my_file = tree.export_graphviz(tree_in_forest, out_file=my_file)
        print getmembers(tree_in_forest.tree_)
        i_tree = i_tree + 1
"""
training_set # list of dict representing the traing set
labels # corresponding labels of the training set
vec = DictVectorizer()
vectorized = vec.fit_transform(training_set)
clf = tree.DecisionTreeClassifier()
clf.fit(vectorized.toarray(), labels)

with open("output.dot", "w") as output_file:
    tree.export_graphviz(clf,
                         feature_names=vec.get_feature_names(),
                         out_file=output_file)


"""

"""
    et = ExtraTreesClassifier(n_estimators=100, max_depth=None, min_samples_split=1, random_state=0)
    columns = ["pclass"]

    labels = train_data["survived"].values
    features = train_data[list(columns)].values

    et_score = cross_val_score(et, features, labels, n_jobs=1).mean()
    print("{0} -> ET: {1})".format(columns, et_score))

    rfc = RandomForestClassifier(n_estimators = 100)
    rfc = rfc.fit(train_data[0::,1::],train_data[0::,0])
    output = rfc.predict(test_data)
"""
